<?php

use App\Http\Controllers\ShortenController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::match(['get', 'post'], '/encode', [ShortenController::class, 'encode'])->name('url.encode');
Route::get('/decode/{shortUrl}', [ShortenController::class, 'decode'])->name('url.decode');
