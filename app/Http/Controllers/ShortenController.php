<?php

namespace App\Http\Controllers;

use App\Services\UrlShortener;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use InvalidArgumentException;

class ShortenController extends Controller
{
    public function __construct(public UrlShortener $urlShortener)
    {
    }

    public function encode(Request $request): JsonResponse
    {
        if (! $request->has('url') || empty($request->get('url'))) {
            throw new InvalidArgumentException('URL must be supplied');
        }

        $url = $request->get('url');

        $shortUrl = $this->urlShortener->encode($url);

        return response()
            ->json(
                [
                    'original-url' => $url,
                    'shortened-url' => $shortUrl,
                    'decode' => route('url.decode', $shortUrl),
                ]
            );
    }

    public function decode($shortUrl): JsonResponse
    {
        try {
            return response()
                ->json(
                    [
                        'short-url' => $shortUrl,
                        'original-url' => $this->urlShortener->decode($shortUrl),
                    ]
                );
        } catch (InvalidArgumentException $e) {
            throw new InvalidArgumentException('Unknown short URL: ' . $shortUrl   );
        }
    }
}
