<?php

namespace App\Services;

use Illuminate\Support\Facades\Cache;
use InvalidArgumentException;

class UrlShortener
{
    public function encode(string $url, int $length = 6 ): string
    {
        $keyForOriginalUrl = $this->makeCacheKeyForOriginalUrl($url);
        if (Cache::has($keyForOriginalUrl)) {
            return Cache::get($keyForOriginalUrl);
        }

        $attemptsRemaining = 50;
        do {
            $shortUrl = '';
            for ($charIndex = 0; $charIndex < $length; $charIndex++) {
                $shortUrl .= $this->getRandomCharacter();
            }
        }
        while ($this->inUse($shortUrl) && --$attemptsRemaining > 0);
        if ($attemptsRemaining === 0) {
            return $this->encode($url, $length + 1);
        }

        Cache::put($this->makeCacheKeyForShortUrl($shortUrl), $url, now()->addWeeks(4));
        Cache::put($keyForOriginalUrl, $shortUrl, now()->addWeeks(4));

        return $shortUrl;
    }

    public function decode($shortUrl): string
    {
        if (Cache::has($this->makeCacheKeyForShortUrl($shortUrl))) {
            return Cache::get($this->makeCacheKeyForShortUrl($shortUrl));
        }

        throw new InvalidArgumentException('No URL found for short URL ' . $shortUrl);
    }

    private function getRandomCharacter(): string
    {
        $allowed = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';

        return substr($allowed, rand(0, strlen($allowed) - 1), 1);
    }

    private function makeCacheKeyForOriginalUrl(string $shortUrl): string
    {
        return 'short-url:original:' . $shortUrl;
    }

    private function makeCacheKeyForShortUrl(string $shortUrl): string
    {
        return 'short-url:short:' . $shortUrl;
    }

    private function inUse(string $shortUrl): bool
    {
        return Cache::has($this->makeCacheKeyForShortUrl($shortUrl));
    }
}
