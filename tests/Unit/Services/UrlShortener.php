<?php

namespace Tests\Unit\Services;

use InvalidArgumentException;
use Tests\TestCase;

class UrlShortener extends TestCase
{
    public function test_encode_returns_a_shortened_url(): void
    {
        $result = app(\App\Services\UrlShortener::class)->encode('http://example.com');

        $this->assertNotNull($result);
        $this->assertEquals(6, strlen($result));
    }

    public function test_encode_returns_the_same_value_for_the_same_url(): void
    {
        $urlShortener = app(\App\Services\UrlShortener::class);
        $result1 = $urlShortener->encode('http://example.com/?foo=bar');
        $result2 = $urlShortener->encode('http://example.com/?ignore=1');
        $result3 = $urlShortener->encode('http://example.com/?foo=bar');

        $this->assertNotEquals($result1, $result2);
        $this->assertEquals($result1, $result3);
    }

    public function test_decode_with_valid_short_url(): void
    {
        $urlShortener = app(\App\Services\UrlShortener::class);
        $testUrl = 'http://example.com/?test=decode';

        $shortUrl = $urlShortener->encode($testUrl);
        $originalUrl = $urlShortener->decode($shortUrl);

        $this->assertEquals($testUrl, $originalUrl  );
    }

    public function test_decode_with_invalid_short_url(): void
    {
        $this->expectExceptionMessage('No URL found for short URL invalid');
        app(\App\Services\UrlShortener::class)->decode('invalid');
    }
}
