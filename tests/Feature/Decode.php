<?php

namespace Tests\Feature;

use App\Services\UrlShortener;
use Tests\TestCase;

class Decode extends TestCase
{
    public function test_decode_with_valid_short_url(): void
    {
        $originalUrl = 'http://example.com/decode=valid';

        $shortUrl = app(UrlShortener::class)->encode($originalUrl);
        $result = $this->get(route('url.decode', ['shortUrl' => $shortUrl]))->json();

        $this->assertEquals(
            [
                'short-url' => $shortUrl,
                'original-url' => $originalUrl,
            ],
            $result
        );
    }

    public function test_decode_with_invalid_short_url(): void
    {
        $this->expectExceptionMessage('Unknown short URL: invalid');
        $this->get(route('url.decode', ['shortUrl' => 'invalid']))->json();
    }
}
