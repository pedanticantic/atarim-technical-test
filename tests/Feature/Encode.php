<?php

namespace Tests\Feature;

use Tests\TestCase;

class Encode extends TestCase
{
    public function test_encode_with_get(): void
    {
        $originalUrl = 'http://example.com/?method=get';
        $result = $this->get(route('url.encode', ['url' => $originalUrl]))->json();

        $this->assertArrayHasKey('shortened-url', $result);
        $this->assertNotEmpty($result['shortened-url']);
        $this->assertEquals(
            [
                'original-url' => 'http://example.com/?method=get',
                'shortened-url' => $result['shortened-url'],
                'decode' => route('url.decode', ['shortUrl' => $result['shortened-url']]),
            ],
            $result
        );
    }

    public function test_encode_with_post(): void
    {
        $originalUrl = 'http://example.com/?method=post';
        $result = $this->post(route('url.encode', ['url' => $originalUrl]))->json();

        $this->assertArrayHasKey('shortened-url', $result);
        $this->assertNotEmpty($result['shortened-url']);
        $this->assertEquals(
            [
                'original-url' => 'http://example.com/?method=post',
                'shortened-url' => $result['shortened-url'],
                'decode' => route('url.decode', ['shortUrl' => $result['shortened-url']]),
            ],
            $result
        );
    }
}
