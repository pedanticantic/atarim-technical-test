# Atarim Technical Test

## Installation

Obviously, check out the repository and run `composer install`.

### Configure

Copy the `.env.example` to `.env` and change/set:
* APP_KEY
* DB_* - just make sure it doesn't crash (the app does not use a database)
* SESSION_DRIVER=file
* CACHE_STORE=file

## Usage

### Start a web server

I ran `php artisan serve` in a terminal to start a web server.

The default URL is `http://localhost:8000/`

### Encode

This endpoint should technically be a POST as it changes the state of the server, but I made it so that either GET or POST can be used.

To encode a URL, send a GET request to `http://localhost:8000/encode?url=<original URL>`

The POST request would be similar.

The endpoint returns a JSON object containing the original URL, the shortened URL and the URL of the relevant "decode" endpoint.

The shortened URL is completely random and does not depend on the supplied URL.

Any shortened URL is cached for 28 days, and making an encode request with a URL the app has seen before (ie is in the cache) will result in the same shortened URL being returned.

### Decode

There is just a GET endpoint for decode.

To decode a shortened URL, send a GET request to `http://localhost:8000/decode/<shortened URL>`.

If the shortened URL is not in the cache, an error is returned.

Otherwise the endpoint returns a JSON object containing the shortened URL and the original URL.

## Tests

The service and the endpoints are covered by tests.

To run a test, execute `php artisan test <test file>` in a terminal, eg `php artisan test tests/Feature/Encode.php`

Some functionality is not tested because it would have taken a long time to set up and would have made the code less clear.

For example, emulating it hitting the cache 50 times with a 6 character shortened URL and ensuring it tries a 7 character string. One way to do it would be to create a "fixture" - a subclass of `UrlShortener` with the `inUse()` overridden where it basically returns true if the parameter is 6 characters long - and using this class in the test.

## Notes

Obviously, this was written under time pressure and I took some short-cuts or compromised in places. For example, I would have created bespoke exception classes for the exceptions that the code throws and as I say above, some functionality is not covered by tests.

I can confirm that I did not get any help from anyone when doing this test nor did I search online for solutions/hints.

## Potential Enhancements

* Put some things in config:
  * default short URL length
  * max cache hits
* Options to clear the cache either for one URL or shortened URL, or all of them (or indeed anything matching a given pattern)
* Allow the FQDN of the shortened URL to be configured
* When encoding, it could (should) ensure the parameter is a valid URL - currently you could give it anything at all!
* Allow for different algorithms for shortening a URL (strategy pattern)
* In terms of future-proofing, there are various places where additional code could be added:
  * in the controllers
  * in the service class
  * in middleware for the endpoints
